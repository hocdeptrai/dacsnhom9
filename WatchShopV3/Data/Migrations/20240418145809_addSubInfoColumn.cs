﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WatchShopV3.Data.Migrations
{
    /// <inheritdoc />
    public partial class addSubInfoColumn : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Styles",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Note",
                table: "Categories",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "Styles");

            migrationBuilder.DropColumn(
                name: "Note",
                table: "Categories");
        }
    }
}
