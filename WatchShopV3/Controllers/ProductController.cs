﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WatchShopV3.Models;
using WatchShopV3.Repositories;

namespace WatchShopV3.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductRepository _productRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IBrandRepository _brandRepository;
        private readonly IStyleRepository _styleRepository;
        public ProductController(IProductRepository productRepository,
        ICategoryRepository categoryRepository, IBrandRepository brandRepository, IStyleRepository styleRepository)
        {
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
            _brandRepository = brandRepository;
            _styleRepository = styleRepository;
        }
        // Hiển thị danh sách sản phẩm
        public async Task<IActionResult> Display(int id)
        {
            var product = await _productRepository.GetByIdAsync(id);
            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }

		public async Task<IActionResult> Index(string searchString)
		{
			var products = _productRepository.GetAll();

			if (!String.IsNullOrEmpty(searchString))
			{
				products = products.Where(s => s.Name.Contains(searchString));
			}

			return View(await products.ToListAsync());
		}
	}
}