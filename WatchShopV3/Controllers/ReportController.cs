﻿using Microsoft.AspNetCore.Mvc;
using WatchShopV3.Data;

namespace WatchShopV3.Controllers
{
    public class ReportController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ReportController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            ViewBag.TongSoSanPhamBanDuoc = _context.OrderDetails.Sum(k => k.Quantity);
            return View();
        }
    }
}
