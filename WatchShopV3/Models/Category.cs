﻿namespace WatchShopV3.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Note { get; set; }

        public List<Product>? Products { get; set;}
	}
}
