﻿namespace WatchShopV3.Models
{
	public class Style
	{
        public int Id { get; set; }
		public string Name { get; set; }

		public string Description { get; set; }

    }
}
