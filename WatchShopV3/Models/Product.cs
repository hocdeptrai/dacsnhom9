﻿using System.ComponentModel.DataAnnotations;

namespace WatchShopV3.Models
{
    public class Product
    {
        public int Id { get; set; }
		
		public string Name { get; set; }

        public string Description { get; set; }

		
		public decimal Price { get; set; }

        public string? ImageUrl { get; set; } // Đường dẫn đến hình ảnh đại diện
        public List<ProductImage>? Images { get; set; }

        public int BrandId { get; set; }
        public Brand? Brand { get; set; }
        
        public int CategoryId { get; set; }

        public Category? Category { get; set; }

        public int StyleId { get; set; }

        public Style? Style { get; set; }

    }
}
