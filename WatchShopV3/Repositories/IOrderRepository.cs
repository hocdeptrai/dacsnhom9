﻿using WatchShopV3.Models;

namespace WatchShopV3.Repositories
{
    public interface IOrderRepository
    {
            Task<IEnumerable<Order>> GetAllAsync();
            Task<Order> GetByIdAsync(int id);
            Task AddAsync(Order category);
            Task UpdateAsync(Order category);
            Task DeleteAsync(int id);
            IEnumerable<OrderDetail> GetOrderDetailsByOrderId(int orderId);
    }
}
