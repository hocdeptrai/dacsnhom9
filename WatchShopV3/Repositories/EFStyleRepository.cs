﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using WatchShopV3.Data;
using WatchShopV3.Models;

namespace WatchShopV3.Repositories
{
	public class EFStyleRepository : IStyleRepository
	{
		private readonly ApplicationDbContext _context;
		public EFStyleRepository(ApplicationDbContext context)
		{
			_context = context;
		}
		public async Task<IEnumerable<Style>> GetAllAsync()
		{
			// return await _context.Products.ToListAsync();
			return await _context.Styles.ToListAsync();
		}
		public async Task<Style> GetByIdAsync(int id)
		{
			// return await _context.Products.FindAsync(id);
			// lấy thông tin kèm theo category
			return await _context.Styles.FirstOrDefaultAsync(p => p.Id == id);
		}
		public async Task AddAsync(Style style)
		{
			_context.Styles.Add(style);
			await _context.SaveChangesAsync();
		}
		public async Task UpdateAsync(Style style)
		{
			_context.Styles.Update(style);
			await _context.SaveChangesAsync();
		}
		public async Task DeleteAsync(int id)
		{
			var style = await _context.Styles.FindAsync(id);
			_context.Styles.Remove(style);
			await _context.SaveChangesAsync();
		}

		public IQueryable<Style> GetAll()
		{
			return _context.Styles;
		}
	}
}
