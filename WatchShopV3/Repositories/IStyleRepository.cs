﻿using WatchShopV3.Models;

namespace WatchShopV3.Repositories
{
	public interface IStyleRepository
	{
		Task<IEnumerable<Style>> GetAllAsync();
		Task<Style> GetByIdAsync(int id);
		Task AddAsync(Style style);
		Task UpdateAsync(Style style);
		Task DeleteAsync(int id);

		IQueryable<Style> GetAll();

    }
}
