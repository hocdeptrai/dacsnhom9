﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WatchShopV3.Models;
using WatchShopV3.Repositories;

namespace WatchShopV3.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class OrderManagerController : Controller
    {
        private readonly IOrderRepository _orderRepository;

        public OrderManagerController(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        // Hiển thị danh sách sản phẩm
        public async Task<IActionResult> Index()
        {
            var orders = await _orderRepository.GetAllAsync();
            return View(orders);
        }
        // Hiển thị form thêm sản phẩm mới

        public async Task<IActionResult> Add()
        {
            return View();
        }
        // Xử lý thêm sản phẩm mới
        [HttpPost]
        public async Task<IActionResult> Add(Order category)
        {
            if (category == null)
            {
                return BadRequest("Category object is null");
            }

            if (ModelState.IsValid)
            {
                await _orderRepository.AddAsync(category);
                return RedirectToAction(nameof(Index));
            }
            return View(category);
        }


        // Hiển thị form cập nhật sản phẩm

        public async Task<IActionResult> Update(int id)
        {
            var category = await _orderRepository.GetByIdAsync(id);
            if (category == null)
            {
                return NotFound();
            }

            return View(category);
        }
        // Xử lý cập nhật sản phẩm
        [HttpPost]
        public async Task<IActionResult> Update(int id, Category category)
        {
            if (ModelState.IsValid)
            {
                var existingCategory = await _orderRepository.GetByIdAsync(id);
                if (existingCategory == null)
                {
                    return NotFound();
                }

                // Cập nhật các thông tin khác của danh mục
                

                await _orderRepository.UpdateAsync(existingCategory);

                return RedirectToAction(nameof(Index));
            }
            return View(category);
        }


        // Hiển thị form xác nhận xóa sản phẩm
        public async Task<IActionResult> Delete(int id)
        {
            var order = await _orderRepository.GetByIdAsync(id);
            if (order == null)
            {
                return NotFound();
            }
            return View(order);
        }

        // Xử lý xóa sản phẩm
        [HttpPost, ActionName("DeleteConfirmed")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _orderRepository.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Display(int id)
        {
            var order = await _orderRepository.GetByIdAsync(id);
            if (order == null)
            {
                return NotFound();
            }
            return View(order);
        }

        public IActionResult Details(int id)
        {
            var orderDetails = _orderRepository.GetOrderDetailsByOrderId(id);

            if (orderDetails == null || !orderDetails.Any())
            {
                return NotFound();
            }

            return View(orderDetails);
        }


    }
}
