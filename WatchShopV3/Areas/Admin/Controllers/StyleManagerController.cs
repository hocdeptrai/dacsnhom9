﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WatchShopV3.Models;
using WatchShopV3.Repositories;

namespace WatchShopV3.Areas.Admin.Controllers
{
	[Area("Admin")]
    [Authorize(Roles = SD.Role_Admin)]
    public class StyleManagerController : Controller
    {
        
		private readonly IStyleRepository _styleRepository;

		public StyleManagerController(IStyleRepository styleRepository)
		{
			_styleRepository = styleRepository;
		}

        // Hiển thị danh sách sản phẩm

        public async Task<IActionResult> Index(string searchString)
        {
            var products = _styleRepository.GetAll();

            if (!String.IsNullOrEmpty(searchString))
            {
                products = products.Where(s => s.Name.Contains(searchString));
            }

            return View(await products.ToListAsync());
        }
        // Hiển thị form thêm sản phẩm mới

        public async Task<IActionResult> Add()
		{
			return View();
		}
		// Xử lý thêm sản phẩm mới

		[HttpPost]
		public async Task<IActionResult> Add(Style style)
		{
			if (style == null)
			{
				return BadRequest("Category object is null");
			}

			if (ModelState.IsValid)
			{
				await _styleRepository.AddAsync(style);
				return RedirectToAction(nameof(Index));
			}
			return View(style);
		}


		// Hiển thị thông tin chi tiết sản phẩm

		public async Task<IActionResult> Display(int id)
		{
			var style = await _styleRepository.GetByIdAsync(id);
			if (style == null)
			{
				return NotFound();
			}
			return View(style);
		}
		// Hiển thị form cập nhật sản phẩm

		public async Task<IActionResult> Update(int id)
		{
			var style = await _styleRepository.GetByIdAsync(id);
			if (style == null)
			{
				return NotFound();
			}

			return View(style);
		}
		// Xử lý cập nhật sản phẩm
		[HttpPost]
		public async Task<IActionResult> Update(int id, Style style)
		{
			if (ModelState.IsValid)
			{
				var existingStyle = await _styleRepository.GetByIdAsync(id);
				if (existingStyle == null)
				{
					return NotFound();
				}

				// Cập nhật các thông tin khác của danh mục
				existingStyle.Name = style.Name;
				existingStyle.Description = style.Description;

                await _styleRepository.UpdateAsync(existingStyle);

				return RedirectToAction(nameof(Index));
			}
			return View(style);
		}


		// Hiển thị form xác nhận xóa sản phẩm
		public async Task<IActionResult> Delete(int id)
		{
			var style = await _styleRepository.GetByIdAsync(id);
			if (style == null)
			{
				return NotFound();
			}
			return View(style);
		}

		// Xử lý xóa sản phẩm
		[HttpPost, ActionName("DeleteConfirmed")]
		public async Task<IActionResult> DeleteConfirmed(int id)
		{
			await _styleRepository.DeleteAsync(id);
			return RedirectToAction(nameof(Index));
		}

    }
}
