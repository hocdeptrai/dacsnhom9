﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WatchShopV3.Models;
using WatchShopV3.Repositories;

namespace WatchShopV3.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles =SD.Role_Admin)]
    public class BrandManagerController : Controller
    {
        private readonly IBrandRepository _brandRepository;

        public BrandManagerController(IBrandRepository brandRepository)
        {
            _brandRepository = brandRepository;
        }

        public async Task<IActionResult> Index(string searchString)
        {
            var categories = _brandRepository.GetAll();

            if (!String.IsNullOrEmpty(searchString))
            {
                categories = categories.Where(s => s.Name.Contains(searchString));
            }

            return View(await categories.ToListAsync());
        }

        public async Task<IActionResult> Add()
        {
            return View();
        }
        // Xử lý thêm sản phẩm mới
        [HttpPost]
        public async Task<IActionResult> Add(Brand brand)
        {
            if (brand == null)
            {
                return BadRequest("Category object is null");
            }

            if (ModelState.IsValid)
            {
                await _brandRepository.AddAsync(brand);
                return RedirectToAction(nameof(Index));
            }
            return View(brand);
        }

        public async Task<IActionResult> Update(int id)
        {
            var brand = await _brandRepository.GetByIdAsync(id);
            if (brand == null)
            {
                return NotFound();
            }

            return View(brand);
        }
        // Xử lý cập nhật sản phẩm

        [HttpPost]
        public async Task<IActionResult> Update(int id, Brand brand)
        {
            if (ModelState.IsValid)
            {
                var existingBrand = await _brandRepository.GetByIdAsync(id);
                if (existingBrand == null)
                {
                    return NotFound();
                }

                // Cập nhật các thông tin khác của danh mục
                existingBrand.Name = brand.Name;
                existingBrand.Description = brand.Description;

                await _brandRepository.UpdateAsync(existingBrand);

                return RedirectToAction(nameof(Index));
            }
            return View(brand);
        }


        // Hiển thị form xác nhận xóa sản phẩm
        public async Task<IActionResult> Delete(int id)
        {
            var category = await _brandRepository.GetByIdAsync(id);
            if (category == null)
            {
                return NotFound();
            }
            return View(category);
        }

        // Xử lý xóa sản phẩm
        [HttpPost, ActionName("DeleteConfirmed")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _brandRepository.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Display(int id)
        {
            var product = await _brandRepository.GetByIdAsync(id);
            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }
    }
}

