﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WatchShopV3.Data;
using WatchShopV3.Models;

namespace WatchShopV3.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = SD.Role_Admin)]
    public class ReportController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ReportController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            ViewBag.TongSoSanPhamBanDuoc = _context.OrderDetails.Sum(k => k.Quantity);
            return View();
        }
    }
}
